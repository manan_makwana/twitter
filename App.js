import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import store from "./Redux/store.js";
import { Provider } from "react-redux";

import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";

import ProfileScreen from "./components/Profile.js";
import TweetsScreen from "./components/tweets.js";
import DetailPage from "./components/viewDetailPage.js";
import EditProfile from "./components/editProfile.js";
import AddTweetForm from "./components/addTweetForm.js";

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const myIcon = <Icon name="rocket" size={30} color="#900" />;

const ProfileStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="Detail Page" component={DetailPage} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
    </Stack.Navigator>
  );
};

const TweetsStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Tweets" component={TweetsScreen} />
    </Stack.Navigator>
  );
};

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Profile"
          screenOptions={({ route }) => ({
            tabBarActiveTintColor: "blue",
            tabBarInactiveTintColor: "black",
            headerShown: false,
            tabBarIcon: ({ focused, size, color }) => {
              let icon;
              if (route.name === "Tweets") {
                icon = "twitter";
                size = focused ? 25 : 20;
              } else if (route.name === "Profile") {
                icon = "user";
                size = focused ? 25 : 20;
              } else if (route.name === "Edit") {
                icon = "edit";
                size = focused ? 25 : 20;
              }
              return <Icon name={icon} size={size} color="#00acee" />;
            },
          })}
        >
          <Tab.Screen name="Profile" component={ProfileStack} />
          <Tab.Screen name="Tweets" component={TweetsStack} />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
