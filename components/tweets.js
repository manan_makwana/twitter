import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { fetchTweets, increaseLike } from "../Redux/actions";
import AddTweetForm from "./addTweetForm";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  Modal,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

function TweetsScreen({ userData, dispatch }) {
  const [page, setPage] = useState(1);
  const [modalVisible, setModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    dispatch(fetchTweets(page, setIsLoading));
  }, [page]);

  const handleLoadMore = () => {
    setPage(page + 1);
  };
  const handleCloseForm = () => {
    setModalVisible(false);
  };
  const renderLoader = () => {
    return isLoading ? (
      <View>
        <ActivityIndicator size="large" color="black" />
      </View>
    ) : null;
  };

  return (
    <SafeAreaView>
      <View style={styles.body}>
        <Modal visible={modalVisible} animationType="slide">
          <MaterialIcons
            name="close"
            size={30}
            onPress={() => setModalVisible(false)}
            style={styles.modalClose}
          />
          <AddTweetForm handleCloseForm={handleCloseForm} />
        </Modal>

        <MaterialIcons
          name="add"
          size={30}
          onPress={() => setModalVisible(true)}
          style={styles.modalAdd}
        />

        <View style={{ height: "90%" }}>
          <FlatList
            data={userData.tweets}
            // keyExtractor={({ id }, index) => id}
            renderItem={({ item, index }) => (
              <View style={styles.card}>
                <Image
                  source={{
                    uri: item.userProfilePic.replace("http", "https"),
                  }}
                  style={styles.image}
                />
                <Text
                  style={{ fontSize: 20, textAlign: "center", color: "white" }}
                >
                  {item.username}
                </Text>
                <Text
                  style={{ fontSize: 10, textAlign: "center", color: "white" }}
                >
                  Created By : {item.createdBy}
                </Text>
                <Text
                  style={{ fontSize: 15, textAlign: "center", color: "white" }}
                >
                  Content : {item.content}
                </Text>
                <View style={styles.addingFlex}>
                  <Text style={{ color: "white" }}>
                    <Icon name="comment" size={15} /> {item.comments}
                  </Text>
                  <Text style={{ color: "white" }}>
                    {item.isLiked === false ? (
                      <Icon
                        name="heart"
                        size={15}
                        onPress={() => dispatch(increaseLike(index))}
                      />
                    ) : (
                      <MaterialIcons name="favorite" size={17} />
                    )}
                    {item.likes}
                  </Text>
                </View>
              </View>
            )}
            ListFooterComponent={renderLoader}
            onEndReached={handleLoadMore}
            onEndReachedThreshold={0}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const mapStateToProps = (state) => {
  return {
    userData: state,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  };
};

const styles = StyleSheet.create({
  body: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "skyblue",
  },
  card: {
    shadowColor: "white",
    shadowRadius: 6,
    shadowOpacity: 0.9,
    // backgroundColor: "#070E7A",
    backgroundColor: "#0D236C",
    marginVertical: 15,
    borderRadius: 10,
    marginHorizontal: 15,
    width: 300,
  },
  addingFlex: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10,
    marginBottom: 5,
  },

  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 5,
    alignSelf: "center",
  },
  modalAdd: {
    marginTop: 10,
    marginBottom: 10,
    borderWidth: 2,
    borderRadius: 10,
    padding: 7,
    borderColor: "#f2f2f2",
  },
  modalClose: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50,
    alignSelf: "center",
    marginBottom: 20,
    borderWidth: 2,
    borderRadius: 10,
    padding: 7,
    borderColor: "#f2f2f2",
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TweetsScreen);
