import React, { useState } from "react";
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  Pressable,
  Image,
  Platform,
} from "react-native";
import { connect } from "react-redux";
import { Formik } from "formik";
import { editProfile } from "../Redux/actions.js";

import { Icon } from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";

import BottomSheet from "reanimated-bottom-sheet";
import Animated from "react-native-reanimated";

import ImagePicker from "react-native-image-crop-picker";

function EditProfile({ userData, dispatch, navigation }) {
  var id = userData.currentUserId;
  var user = userData.users[id];

  const [image, setImage] = useState(user.profilePic);

  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image);
      setImage(image.path);
      bs.current.snapTo(1);
    });
  };

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image);
      setImage(image.path);
      bs.current.snapTo(1);
    });
  };

  const renderInner = () => (
    <View style={styles.panel}>
      <View style={{ alignItems: "center" }}>
        <Text style={styles.panelTitle}>Upload Photo</Text>
        <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
      </View>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={takePhotoFromCamera}
      >
        <Text style={styles.panelButtonTitle}>Take Photo</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={choosePhotoFromLibrary}
      >
        <Text style={styles.panelButtonTitle}>Choose From Library</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={() => bs.current.snapTo(1)}
      >
        <Text style={styles.panelButtonTitle}>Cancel</Text>
      </TouchableOpacity>
    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  const bs = React.createRef();
  const fall = new Animated.Value(1);

  return (
    <View>
      <Formik
        initialValues={{
          username: user.name,
          createdBy: user.username,
          content: user.bio,
          // image: user.profilePic,
        }}
        onSubmit={(values) => {
          values.profilePic = image;
          dispatch(editProfile(values));
          navigation.navigate("DetailPage", { item: userData.users[id] });
        }}
      >
        {(props) => (
          <View style={{ marginTop: 30 }}>
            <BottomSheet
              ref={bs}
              snapPoints={[0, -400]}
              renderContent={renderInner}
              renderHeader={renderHeader}
              initialSnap={1}
              callbackNode={fall}
              enabledGestureInteraction={true}
            />
            <Pressable onPress={() => bs.current.snapTo(0)}>
              <Image
                source={{ uri: image.replace("http", "https") }}
                // size={80}
                style={{
                  height: 100,
                  width: 100,
                  alignSelf: "center",
                  marginBottom: 20,
                  borderRadius: 100,
                }}
              />
            </Pressable>
            <TextInput
              style={styles.inputBox}
              placeholder="UserName"
              onChangeText={props.handleChange("username")}
              value={props.values.username}
            />
            <TextInput
              style={styles.inputBox}
              placeholder="Created By"
              onChangeText={props.handleChange("createdBy")}
              value={props.values.createdBy}
            />
            <TextInput
              multiline
              style={styles.inputBox}
              placeholder="Content"
              onChangeText={props.handleChange("content")}
              value={props.values.content}
            />

            <Button
              title="Submit"
              color="maroon"
              onPress={props.handleSubmit}
              style={{ marginTop: 10 }}
            />
          </View>
          // <View style={styles.container}>
          //   <BottomSheet
          //     ref={bs}
          //     snapPoints={[330, 0]}
          //     renderContent={renderInner}
          //     renderHeader={renderHeader}
          //     initialSnap={1}
          //     callbackNode={fall}
          //     enabledGestureInteraction={true}
          //   />
          //   <Animated.View
          //     style={{
          //       margin: 20,
          //       opacity: Animated.add(0.1, Animated.multiply(fall, 1.0)),
          //     }}
          //   >
          //     <View style={{ alignItems: "center" }}>
          //       <TouchableOpacity onPress={() => bs.current.snapTo(0)}>
          //         <View
          //           style={{
          //             height: 100,
          //             width: 100,
          //             borderRadius: 15,
          //             justifyContent: "center",
          //             alignItems: "center",
          //           }}
          //         >
          //           <ImageBackground
          //             source={{
          //               uri: props.values.image.replace("http", "https"),
          //             }}
          //             style={{ height: 100, width: 100 }}
          //             imageStyle={{ borderRadius: 15 }}
          //           >
          //             <View
          //               style={{
          //                 flex: 1,
          //                 justifyContent: "center",
          //                 alignItems: "center",
          //               }}
          //             >
          //               <Icon
          //                 name="camera"
          //                 size={35}
          //                 color="#fff"
          //                 style={{
          //                   opacity: 0.7,
          //                   alignItems: "center",
          //                   justifyContent: "center",
          //                   borderWidth: 1,
          //                   borderColor: "#fff",
          //                   borderRadius: 10,
          //                 }}
          //               />
          //             </View>
          //           </ImageBackground>
          //         </View>
          //       </TouchableOpacity>
          //     </View>
          //     <View style={styles.action}>
          //       <FontAwesome name="user-o" size={20} />
          //       <TextInput
          //         style={styles.textInput}
          //         placeholder="UserName"
          //         placeholderTextColor="#666666"
          //         autoCorrect={false}
          //         onChangeText={props.handleChange("username")}
          //         value={props.values.username}
          //       />
          //     </View>
          //     <View style={styles.action}>
          //       <FontAwesome name="user-o" size={20} />
          //       <TextInput
          //         style={styles.textInput}
          //         placeholder="Created By"
          //         onChangeText={props.handleChange("createdBy")}
          //         value={props.values.createdBy}
          //       />
          //     </View>
          //     <View style={styles.action}>
          //       <FontAwesome name="user-o" size={20} />
          //       <TextInput
          //         style={styles.textInput}
          //         placeholderTextColor="#666666"
          //         autoCorrect={false}
          //         multiline
          //         placeholder="Content"
          //         onChangeText={props.handleChange("content")}
          //         value={props.values.content}
          //       />
          //     </View>
          //     <TouchableOpacity style={styles.commandButton} onPress={() => {}}>
          //       <Text style={styles.panelButtonTitle}>Submit</Text>
          //     </TouchableOpacity>
          //   </Animated.View>
          // </View>
        )}
      </Formik>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    userData: state,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  };
};

const styles = StyleSheet.create({
  action: {
    flexDirection: "row",
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  container: {
    flex: 1,
  },
  commandButton: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: "#FF6347",
    alignItems: "center",
    marginTop: 10,
  },
  inputBox: {
    borderWidth: 2,
    borderColor: "#ddd",
    padding: 10,
    fontSize: 20,
    borderRadius: 10,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  panel: {
    padding: 20,
    backgroundColor: "#FFFFFF",
    paddingTop: 20,
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    // shadowColor: '#000000',
    // shadowOffset: {width: 0, height: 0},
    // shadowRadius: 5,
    // shadowOpacity: 0.4,
  },
  header: {
    backgroundColor: "#FFFFFF",
    shadowColor: "#333333",
    shadowOffset: { width: -1, height: -3 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: "gray",
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: "#FF6347",
    alignItems: "center",
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "white",
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
