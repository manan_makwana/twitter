import React from "react";
import { StyleSheet, Button, TextInput, View, Text } from "react-native";
import { Formik } from "formik";
import { addTweet } from "../Redux/actions";
import { connect } from "react-redux";

function AddTweetForm(props) {
  return (
    <View>
      <Formik
        initialValues={{
          userProfilePic: "",
          username: "",
          createdBy: "",
          content: "",
          comments: "",
          likes: "",
          isLiked: false,
        }}
        onSubmit={(values) => {
          values.userProfilePic = "http://placeimg.com/640/480/people";
          values.likes = "0";
          values.comments = "0";
          values.isLiked = false;
          props.dispatch(addTweet(values));
          props.handleCloseForm();
        }}
      >
        {(props) => (
          <View>
            <TextInput
              style={styles.inputBox}
              placeholder="UserName"
              onChangeText={props.handleChange("username")}
              value={props.values.username}
            />
            <TextInput
              style={styles.inputBox}
              placeholder="Created By"
              onChangeText={props.handleChange("createdBy")}
              value={props.values.createdBy}
            />
            <TextInput
              multiline
              style={styles.inputBox}
              placeholder="Content"
              onChangeText={props.handleChange("content")}
              value={props.values.content}
            />

            <Button
              title="submit"
              color="maroon"
              onPress={props.handleSubmit}
              style={{ marginTop: 10 }}
            />
          </View>
        )}
      </Formik>
    </View>
  );
}

const mapDispatchToProps = (dispatch) => {
  return { dispatch: dispatch };
};

const styles = StyleSheet.create({
  inputBox: {
    borderWidth: 2,
    borderColor: "#ddd",
    padding: 10,
    fontSize: 20,
    borderRadius: 10,
    marginBottom: 10,
    marginHorizontal: 10,
  },
});

export default connect(null, mapDispatchToProps)(AddTweetForm);
