import React, { useState } from "react";
import {
  View,
  Text,
  SafeAreaView,
  Button,
  StyleSheet,
  Image,
  Modal,
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import EditProfile from "./editProfile";

function DetailPage({ navigation, route }) {
  const [modalVisible, setModalVisible] = useState(false);
  var data = route.params?.item;
  const handleCloseForm = () => {
    console.warn("handleCloseForm");
    setModalVisible(false);
  };
  return (
    <SafeAreaView style={{ backgroundColor: "skyblue", flex: 1 }}>
      <View style={styles.profile}>
        <Image
          source={{
            uri: data.profilePic.replace("http", "https"),
          }}
          style={styles.imageStyling}
        />
        <Text style={styles.name}>{data.name}</Text>
        <View style={styles.particularDetail}>
          <MaterialIcons name="person" size={30} color="white" />
          <Text style={styles.textStyle}>{data.username}</Text>
        </View>
        <View style={styles.particularDetail}>
          <MaterialIcons name="place" size={30} color="white" />
          <Text style={styles.textStyle}>
            {data.address}, {data.address2}
          </Text>
        </View>
        <View style={styles.particularDetail}>
          <Text style={styles.textStyle}>Pincode : {data.pincode}</Text>
        </View>

        <View style={styles.particularDetail}>
          <MaterialIcons name="message" size={30} color="white" />
          <Text style={styles.textStyle}>{data.bio}</Text>
        </View>
        <View style={styles.particularDetail}>
          <MaterialIcons name="mail-outline" size={30} color="white" />
          <Text style={styles.textStyle}>{data.email}</Text>
        </View>

        <View style={styles.addingBorder}>
          <View style={styles.addingFlex}>
            <Text style={styles.connectionDetail}>
              Following : {data.following}
            </Text>
            <Text style={styles.connectionDetail}>
              Followers : {data.followers}
            </Text>
          </View>
        </View>

        <Modal visible={modalVisible} animationType="slide">
          <MaterialIcons
            name="close"
            size={30}
            onPress={() => setModalVisible(false)}
            style={styles.modalClose}
          />
          <EditProfile handleCloseForm={handleCloseForm} />
        </Modal>

        {/* <MaterialIcons
          name="add"
          size={30}
          onPress={() => setModalVisible(true)}
          style={styles.modalAdd}
        /> */}
        <Text
          style={styles.modalAdd}
          onPress={() =>
            navigation.navigate("EditProfile", {
              handleCloseForm: handleCloseForm,
            })
          }
        >
          Edit Profile
        </Text>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  addingBorder: {
    borderTopColor: "grey",
    borderTopWidth: 1,
    marginTop: 45,
    borderRadius: 15,
    borderBottomColor: "grey",
    borderBottomWidth: 1,
  },
  addingFlex: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20,
    marginBottom: 20,
  },
  connectionDetail: {
    color: "white",
    fontWeight: "300",
    fontSize: 20,
  },
  imageStyling: {
    width: 140,
    height: 140,
    borderRadius: 50,
    marginTop: 30,
    alignSelf: "center",
  },
  modalAdd: {
    color: "white",
    fontSize: 25,
    marginTop: 30,
    textAlign: "center",
  },
  modalClose: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50,
    alignSelf: "center",
    marginBottom: 20,
    borderWidth: 2,
    borderRadius: 10,
    padding: 7,
    borderColor: "#f2f2f2",
  },
  name: {
    fontSize: 25,
    textAlign: "center",
    marginTop: 20,
    marginBottom: 15,
    fontWeight: "600",
    color: "white",
  },
  particularDetail: {
    textAlign: "left",
    marginTop: 15,
    marginLeft: 10,
    flexDirection: "row",
  },
  profile: {
    shadowColor: "white",
    shadowRadius: 6,
    shadowOpacity: 0.9,
    // backgroundColor: "#070E7A",
    backgroundColor: "#0D236C",
    borderRadius: 15,
    // marginHorizontal: 25,
    marginTop: 20,
    height: "95%",
    width: "95%",
    alignSelf: "center",
  },
  textStyle: {
    fontSize: 18,
    color: "white",
    padding: 3,
    marginLeft: 2,
  },
});

export default DetailPage;
