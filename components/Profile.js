import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { fetchUsers } from "../Redux/actions";
import { TouchableOpacity } from "react-native-gesture-handler";
import {
  Button,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  Alert,
} from "react-native";

function ProfileScreen({ userData, dispatch, navigation }) {
  useEffect(() => {
    dispatch(fetchUsers());
  }, []);
  // console.warn(userData);
  // var mainProfile = userData.users[0];
  var mainProfile = {
    name: "Priscilla Gleichner",
    address: "Ebert Ramp",
    address2: "Apt. 823",
    pincode: "05671",
    bio: "Right-sized zero tolerance complexity",
    followers: 65611,
    following: 57108,
    username: "Orland75",
    email: "Ron.Hagenes@yahoo.com",
    profilePic: "http://placeimg.com/640/480/people",
    id: "1",
  };
  return (
    <SafeAreaView>
      <View style={styles.body}>
        <View style={styles.profile}>
          <Image
            source={{
              uri: mainProfile.profilePic.replace("http", "https"),
            }}
            style={styles.mainCardImage}
          />
          <Text
            style={[
              styles.mainCardText,
              {
                fontSize: 25,
                fontWeight: "600",
              },
            ]}
          >
            {mainProfile.name}
          </Text>
          <Text
            style={[
              styles.mainCardText,
              {
                fontSize: 15,
              },
            ]}
          >
            {mainProfile.email}
          </Text>
          <Text style={styles.mainCardText}>{mainProfile.bio}</Text>
          <View style={styles.border}>
            <View style={styles.addingFlex}>
              <Text style={styles.connectionDetail}>
                Following : {mainProfile.following}
              </Text>
              <Text style={styles.connectionDetail}>
                Followers : {mainProfile.followers}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.mainCard}>
          <View>
            <Text style={styles.secondaryCardHeader}>
              People You May Follow
            </Text>
          </View>
          <FlatList
            data={userData.users}
            renderItem={({ item }) => (
              <View style={styles.card}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-around",
                  }}
                >
                  <Image
                    source={{
                      uri: item.profilePic.replace("http", "https"),
                    }}
                    style={styles.secondaryCardImage}
                  />
                  <View style={{ flexDirection: "column" }}>
                    <Text
                      style={{
                        fontSize: 20,
                        textAlign: "center",
                        marginTop: 7,
                      }}
                    >
                      {item.name}
                    </Text>
                    <Text style={{ fontSize: 15, textAlign: "center" }}>
                      @{item.username}
                    </Text>
                  </View>
                  <View>
                    <Button
                      title="Follow"
                      onPress={() => {
                        Alert.alert("You have followed him");
                      }}
                    />
                    <Text
                      style={{
                        textAlign: "center",
                        color: "blue",
                        fontSize: 15,
                      }}
                      onPress={() => {
                        navigation.navigate("Detail Page", { item: item });
                      }}
                    >
                      more details...
                    </Text>
                  </View>
                </View>
              </View>
            )}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const mapStateToProps = (state) => {
  return {
    userData: state,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
    // fetchUsers: () => {
    //   dispatch(fetchUsers());
    // },
  };
};

const styles = StyleSheet.create({
  addingFlex: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20,
    marginBottom: 20,
  },
  body: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "skyblue",
    flexDirection: "column",
  },
  border: {
    borderTopColor: "grey",
    borderTopWidth: 1,
    marginTop: 18,
    borderRadius: 15,
    borderBottomColor: "grey",
    borderBottomWidth: 1,
  },
  card: {
    shadowColor: "white",
    shadowRadius: 6,
    // shadowOpacity: 0.1,
    backgroundColor: "skyblue",
    marginVertical: 15,
    borderRadius: 15,
    marginHorizontal: 15,
  },
  connectionDetail: {
    color: "white",
    fontWeight: "300",
    fontSize: 20,
  },

  mainCard: {
    shadowColor: "white",
    shadowRadius: 6,
    shadowOpacity: 0.9,
    // backgroundColor: "#070E7A",
    backgroundColor: "#0D236C",
    marginVertical: 15,
    borderRadius: 15,
    marginHorizontal: 15,
    height: "42.5%",
  },
  mainCardImage: {
    width: 130,
    height: 130,
    borderRadius: 50,
    marginTop: 15,
    marginBottom: 20,
    alignSelf: "center",
  },
  mainCardText: {
    fontSize: 20,
    textAlign: "center",
    color: "white",
    marginBottom: 5,
  },
  profile: {
    shadowColor: "white",
    shadowRadius: 6,
    shadowOpacity: 0.9,
    // backgroundColor: "#070E7A",
    backgroundColor: "#0D236C",
    borderRadius: 15,
    marginHorizontal: 15,
    marginTop: 20,
    height: "51%",
    width: "92%",
  },
  secondaryCardHeader: {
    fontSize: 25,
    fontWeight: "700",
    marginTop: 20,
    marginBottom: 10,
    textAlign: "center",
    color: "white",
  },
  secondaryCardImage: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginVertical: 5,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
