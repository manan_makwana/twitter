import {
  FETCH_USERS_SUCCESS,
  FETCH_TWEETS_SUCCESS,
  ADD_TWEET,
  INCREASE_LIKE,
  EDIT_PROFILE,
} from "./types";

const initialState = {
  users: [],
  tweets: [],
  currentUserId: 0,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_SUCCESS: {
      return {
        ...state,
        users: action.payload,
      };
    }
    case FETCH_TWEETS_SUCCESS: {
      return {
        ...state,
        tweets: [...state.tweets, ...action.payload],
      };
    }
    case ADD_TWEET: {
      var arr = state.tweets.slice();
      return {
        ...state,
        tweets: [action.payload, ...arr],
      };
    }
    case INCREASE_LIKE: {
      var id = action.payload;
      var temp = state.tweets.slice();
      temp[id].likes = parseInt(temp[id].likes) + 1;
      temp[id].isLiked = true;
      return {
        ...state,
        tweets: temp,
      };
    }
    case EDIT_PROFILE: {
      var arr = state.users.slice();
      var newData = action.payload;
      const currentUserId = state.currentUserId;
      arr[currentUserId].name = newData.username;
      arr[currentUserId].username = newData.createdBy;
      arr[currentUserId].bio = newData.content;
      arr[currentUserId].profilePic = newData.profilePic;
      // console.warn(arr);
      return {
        ...state,
        users: arr,
      };
    }
    default:
      return state;
  }
};

export default reducer;
