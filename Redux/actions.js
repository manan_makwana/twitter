import {
  FETCH_USERS_SUCCESS,
  FETCH_TWEETS_SUCCESS,
  ADD_TWEET,
  INCREASE_LIKE,
  EDIT_PROFILE,
} from "./types";

export const fetchUsersSuccess = (users) => {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: users,
  };
};

export const fetchTweetsSuccess = (tweets) => {
  // console.warn("inside success", tweets);
  return {
    type: FETCH_TWEETS_SUCCESS,
    payload: tweets,
  };
};

export const addTweet = (newTweet) => {
  // console.warn(newTweet);
  return {
    type: ADD_TWEET,
    payload: newTweet,
  };
};

export const editProfile = (newData) => {
  // console.warn("inside actions");
  return {
    type: EDIT_PROFILE,
    payload: newData,
  };
};

export const increaseLike = (id) => {
  return {
    type: INCREASE_LIKE,
    payload: id,
  };
};

export const fetchUsers = () => {
  return (dispatch) => {
    fetch("https://5fcaaa4b3c1c220016442a9f.mockapi.io/api/v1/users")
      .then((res) => res.json())
      .then((res) => {
        const users = res;
        if (users !== []) {
          dispatch(fetchUsersSuccess(users));
        }
      })
      .catch((e) => {
        const error = e.message;
        console.warn(error);
      });
  };
};

export const fetchTweets = (page, setIsLoading) => {
  setIsLoading(true);
  return (dispatch) => {
    fetch(
      `https://5fcaaa4b3c1c220016442a9f.mockapi.io/api/v1/tweets/?page=${page}&limit=5`
    )
      .then((res) => res.json())
      .then((res) => {
        var tweets = res;
        if (tweets !== []) {
          tweets = tweets.map((element) => {
            return { ...element, isLiked: false };
          });
          // console.warn("form action", tweets);
          dispatch(fetchTweetsSuccess(tweets));
          setIsLoading(false);
        }
      })
      .catch((e) => {
        const error = e.message;
        console.warn(error);
      });
  };
};
